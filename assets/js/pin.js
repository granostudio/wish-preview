var SM = {
    sceneHeights: [], // has to be initialized with the scene heights
    scenePinLengths: [], // has to be initilized with "0"s
    mainPinEl: "#main-pin",
    sceneSelector: ".scene",
    smController: new ScrollMagic.Controller()
  };
  
  $(document).ready(function ($) {
    initScrollMagic();
  });
  
  function initScrollMagic() {
    var scenes = $(SM.sceneSelector); // expecting scene wrapper elements with the class 'scene'
  
    for (var i = 0; i < scenes.length; i++) {
      SM.sceneHeights.push($(scenes[i]).height()); // init scene heights
      SM.scenePinLengths.push(0); // init scene pin duration array
    }
  
    console.log("Scene Heights are", SM.sceneHeights);
  
    // seperate functions for each scene makes sense, cause of individual TweenMax animations. Could maybe be improved by an factory function, that gets the TweenMax/TimelineMax object.
    initPinScene1(0); // give sceneIndex as argument. sceneIndex starting with 0.
    initPinScene2(1);
    initPinScene3(2);
    initPinScene4(3);
  }
  
  
  // Helper Function
  function _sceneOffset(sceneIndex) {
    var total = 0;
    var sceneHeightsForOffset = SM.sceneHeights.slice(0, sceneIndex);
    var scenePinsForOffset = SM.scenePinLengths.slice(0, sceneIndex);
  
    for (var i = 0; i < sceneHeightsForOffset.length; i++) {
      total += sceneHeightsForOffset[i] + scenePinsForOffset[i];
    }
  
    return total;
  }
  
  
  // Scene 1
  function initPinScene1(sceneIndex) {
    console.log("Scene Offset for sceneIndex "+ sceneIndex +" is: "+ _sceneOffset(sceneIndex));
    var pinLength = 400;
    var newSMScene = new ScrollMagic.Scene({
      triggerHook: 0,
      duration: pinLength,
      offset: _sceneOffset(sceneIndex)
    }).setPin(SM.mainPinEl, {pushFollowers: false})
        .setTween(new TimelineMax())
        .addIndicators({name: "pin scene 1"})
        .addTo(SM.smController); // assign the scene to the controller
  
    SM.scenePinLengths[sceneIndex] = pinLength; // updating the scene pinning length
  }
  
  // Scene 2
  function initPinScene2(sceneIndex) {
    console.log("Scene Offset for sceneIndex "+ sceneIndex +" is: "+ _sceneOffset(sceneIndex));
    var pinLength = 800;
    var newSMScene = new ScrollMagic.Scene({
      triggerHook: 0,
      duration: pinLength,
      offset: _sceneOffset(sceneIndex)
    }).setPin(SM.mainPinEl, {pushFollowers: false})
        // .setTween(new TimelineMax())
        
        .setClassToggle(".text-block", "slide-right")
        // .setTween(new TweenMax.fromTo(".text-block",1,{css:{y:"0px",opacity:'0' ,  ease: Linear.easeNone}},{css:{y:"100px",opacity:'1' ,  ease: Linear.easeNone}}))
       
        .addIndicators({name: "pin scene 2"})
        .addTo(SM.smController); // assign the scene to the controller
  
    SM.scenePinLengths[sceneIndex] = pinLength; // updating the scene pinning length
  }
  
  // Scene 3
  function initPinScene3(sceneIndex) {
    console.log("Scene Offset for sceneIndex "+ sceneIndex +" is: "+ _sceneOffset(sceneIndex));
    var pinLength = 800;
    var newSMScene = new ScrollMagic.Scene({
      triggerHook: 0,
      duration: pinLength,
      offset: _sceneOffset(sceneIndex)
    }).setPin(SM.mainPinEl, {pushFollowers: false})
        .setTween(new TimelineMax())
        .addIndicators({name: "pin scene 3"})
        .addTo(SM.smController); // assign the scene to the controller
  
    SM.scenePinLengths[sceneIndex] = pinLength; // updating the scene pinning length
  }
  
  // Scene 4
  function initPinScene4(sceneIndex) {
    console.log("Scene Offset for sceneIndex "+ sceneIndex +" is: "+ _sceneOffset(sceneIndex));
    var pinLength = 1000;
    var newSMScene = new ScrollMagic.Scene({
      triggerHook: 0,
      duration: pinLength,
      offset: _sceneOffset(sceneIndex)
    }).setPin(SM.mainPinEl, {pushFollowers: false})
        .setTween(new TimelineMax())
        .addIndicators({name: "pin scene 4"})
        .addTo(SM.smController); // assign the scene to the controller
  
    SM.scenePinLengths[sceneIndex] = pinLength; // updating the scene pinning length
  }
  
  